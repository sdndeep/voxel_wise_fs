from keras.layers import Input, Conv3D, MaxPooling3D, Dropout, BatchNormalization, Reshape, Dense, ELU, concatenate, add, Lambda, MaxPooling2D
from keras.models import Model
from keras.optimizers import Adam
from keras import backend as K
import tensorflow.compat.v1 as tf
from keras.callbacks import LearningRateScheduler
from keras.callbacks import EarlyStopping
from keras.callbacks import Callback

from keras.metrics import binary_crossentropy

import numpy as np
import math
from keras.models import load_model


from sklearn.metrics import roc_auc_score

from utils.sepconv3D_py3 import SeparableConv3D
from utils.augmentation import CustomIterator

tf.disable_v2_behavior()


class Parameters():
    def __init__ (self, param_dict):
        self.w_regularizer = param_dict['w_regularizer']
        self.batch_size = param_dict['batch_size']
        self.drop_rate = param_dict['drop_rate']
        self.epochs = param_dict['epochs']
        self.gpu = param_dict['gpu']
        self.model_filepath = param_dict['model_filepath'] + '/net.h5'
        self.num_clinical = param_dict['num_clinical']
        self.image_shape = param_dict['image_shape']        
        
class Net ():
    def __init__ (self, params):
        self.params = params
        
        self.mri = Input (shape = (self.params.image_shape), dtype = tf.float32)
        
        
        
        xalex3D = XAlex3D(w_regularizer = self.params.w_regularizer, drop_rate = self.params.drop_rate)
    
        with tf.device(self.params.gpu):
            self.fc_mci = xalex3D (self.mri)
            self.output_mci = Dense(units = 1, activation = 'sigmoid', name = 'mci_output') (self.fc_mci)
    
    def auc_roc(self, y_true, y_pred):
        
        value, update_op = tf.contrib.metrics.streaming_auc(y_pred, y_true)

        # find all variables created for this metric
        metric_vars = [i for i in tf.local_variables() if 'auc_roc' in i.name.split('/')[1]]

        # Add metric variables to GLOBAL_VARIABLES collection.
        # They will be initialized for new session.
        for v in metric_vars:
            tf.add_to_collection(tf.GraphKeys.GLOBAL_VARIABLES, v)

        # force to update metric values
        with tf.control_dependencies([update_op]):
            value = tf.identity(value)
            return value
        
    def get_lr_metric(optimizer): 
            def lr(y_true,y_pred):
                return optimizer.lr
            return lr
    
    def train (self, data):
        train_data, val_data = data
        train_samples = train_data[0].shape[0]
        val_samples = val_data[0].shape[0]
        
        
        data_flow_train = CustomIterator (train_data, batch_size = self.params.batch_size,
                                          shuffle = True)
        data_flow_val = CustomIterator (val_data, batch_size = self.params.batch_size,
                                          shuffle = True)
        
                

        self.model = Model(inputs = [self.mri], outputs = self.output_mci)    
        self.model.summary()
        
        def get_lr_metric(optimizer): 
            def lr(y_true,y_pred):
                return optimizer.lr
            return lr
        
        
        
        
        

        
        lrate = LearningRateScheduler(step_decay)                                              
        optimizer = Adam(lr=0.001) 
        lr_metric = get_lr_metric(optimizer)                                                                  
        
        
       
        
        self.model.compile(optimizer = optimizer, loss = [binary_crossentropy], metrics =['acc'])#, auc_roc])#, lr_metric])  # #controlla sugli altri models
        
        
        es = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience = 10, baseline = 15, restore_best_weights = True)
        
        callback = [lrate, es]
                                                                                
        
        
        history = self.model.fit (data_flow_train,
                   steps_per_epoch = train_samples/self.params.batch_size, 
                   epochs = self.params.epochs,
                   callbacks = callback,
                   shuffle = True,
                   validation_data = data_flow_val,
                   validation_steps = val_samples/self.params.batch_size
                   
                   )
        
        self.model.save("master/saved_models/model_{}.hd5".format(self.params.epochs))
        
        
        
        return history.history

    

    def predict (self, data_test):
        test_mri, test_labels = data_test#test_jac, test_xls, test_labels = data_test 
        preds = self.model.predict ([test_mri])#, test_jac, test_xls])

        return preds

    def evaluate (self, data_test):
        test_mri, test_labels = data_test#, test_jac, test_xls, test_labels = data_test
        metrics = self.model.evaluate (x = [test_mri], y = test_labels, batch_size = self.params.batch_size)#, test_jac, test_xls], y = test_labels, batch_size = self.params.batch_size)
        return metrics



def XAlex3D(w_regularizer = None, drop_rate = 0.): #see Simeon Spasov et al. 2019 for further details
  
    #3D Multi-modal deep learning neural network (refer to fig. 4 for chain graph of architecture)
    def f(mri_volume):
    
        #First conv layers
        conv1_left = _conv_bn_relu_pool_drop(24, 11, 13, 11, strides = (4, 4, 4), w_regularizer = w_regularizer,drop_rate = drop_rate, pool=True, padding = 'same') (mri_volume)

        conv1_right = _conv_bn_relu_pool_drop(24, 11, 13, 11, strides = (4, 4, 4), w_regularizer = w_regularizer,drop_rate = drop_rate, pool=True, padding = 'same') (mri_volume)
    
        #Second layer
        conv2_left =_conv_bn_relu_pool_drop(48, 5, 6, 5, w_regularizer = w_regularizer,  drop_rate = drop_rate, pool=True) (conv1_left)
    
        conv2_right =_conv_bn_relu_pool_drop(48, 5, 6, 5, w_regularizer = w_regularizer,  drop_rate = drop_rate, pool=True) (conv1_right)# modificato
    
        conv2_concat = concatenate([conv2_left, conv2_right], axis = -1)
    
        #Introduce Middle Flow (separable convolutions with a residual connection)
        conv_mid_1 = mid_flow (conv2_concat, drop_rate, w_regularizer, filters = 96)
    
        #Split channels for grouped-style convolution
        conv_mid_1_1= Lambda (lambda x:x[:,:,:,:,:48]) ( conv_mid_1 )
        conv_mid_1_2 = Lambda (lambda x:x[:,:,:,:,48:]) (conv_mid_1 )
        
        conv5_left = _conv_bn_relu_pool_drop (24, 3, 4, 3, w_regularizer = w_regularizer,  drop_rate = drop_rate, pool=True) (conv_mid_1_1)
    
        conv5_right = _conv_bn_relu_pool_drop (24, 3, 4, 3, w_regularizer = w_regularizer,  drop_rate = drop_rate, pool=True) (conv_mid_1_2)
    
        conv6_left = _conv_bn_relu_pool_drop (8, 3, 4, 3, w_regularizer = w_regularizer,drop_rate = drop_rate, pool=True) (conv5_left)

        conv6_right = _conv_bn_relu_pool_drop (8, 3, 4, 3, w_regularizer = w_regularizer,drop_rate = drop_rate, pool=True) (conv5_right)
        
        conv6_concat = concatenate([conv6_left, conv6_right], axis = -1)
    
    
        #Flatten 3D conv network representations
        flat_conv_6 = Reshape((np.prod(K.int_shape(conv6_concat)[1:]),))(conv6_concat)
    

        
    
        fc1 = _fc_bn_relu_drop (10, w_regularizer, drop_rate = drop_rate) (flat_conv_6)
        
    
        #Final 4D embedding
        fc2 = _fc_bn_relu_drop (4, w_regularizer, drop_rate = drop_rate) (fc1) 

      
        return fc2
    return f



def _fc_bn_relu_drop (units, w_regularizer = None, drop_rate = 0., name = None):
    #Defines Fully connected block 
    def f(input):  
        fc = Dense(units = units, activation = 'linear', kernel_regularizer=w_regularizer, name = name, kernel_initializer = 'random_uniform', bias_initializer = 'zeros') (input) 
        fc = BatchNormalization()(fc)
        fc = ELU()(fc)
        fc = Dropout (drop_rate) (fc)
        return fc
    return f

def _conv_bn_relu_pool_drop(filters, height, width, depth, strides=(1, 1, 1), padding = 'same', w_regularizer = None, 
                            drop_rate = None, name = None, pool = False):
   #Defines convolutional block 
   def f(input):
       conv = Conv3D(filters, (height, width, depth),
                             strides = strides, kernel_initializer="he_normal",
                             padding=padding, kernel_regularizer = w_regularizer, name = name)(input)
       norm = BatchNormalization()(conv)
       elu = ELU()(norm)
       if pool == True:       
           elu = MaxPooling3D(pool_size=3, strides=2) (elu)
       return Dropout(drop_rate) (elu)
   return f

def _sepconv_bn_relu_pool_drop (filters, height, width, depth, strides = (1, 1, 1), padding = 'same', depth_multiplier = 1, w_regularizer = None, 
                            drop_rate = None, name = None, pool = False):
    #Defines separable convolutional block 
    def f (input):
        sep_conv = SeparableConv3D(filters, (height, width, depth),
                             strides = strides, depth_multiplier = depth_multiplier,kernel_initializer="he_normal",
                             padding=padding, kernel_regularizer = w_regularizer, name = name, use_bias = True)(input)
        sep_conv = BatchNormalization()(sep_conv)
        elu = ELU()(sep_conv)
        if pool == True:       
           elu = MaxPooling2D(pool_size=3, strides=2, padding = 'same') (elu)
        return Dropout(drop_rate) (elu)
    return f


def mid_flow (x, drop_rate, w_regularizer, filters = 96):
    #3 consecutive separable blocks with a residual connection 
    residual = x    
    x = _sepconv_bn_relu_pool_drop (filters, 3, 3, 3, padding='same', drop_rate=drop_rate, w_regularizer = w_regularizer )(x) 
    x = _sepconv_bn_relu_pool_drop (filters, 3, 3, 3, padding='same', drop_rate=drop_rate, w_regularizer = w_regularizer )(x)
    x = _sepconv_bn_relu_pool_drop (filters, 3, 3, 3, padding='same', drop_rate=drop_rate, w_regularizer = w_regularizer)(x)
    x = add([x, residual])
    return x

def step_decay (epoch):
    #Decaying learning rate function
    initial_lrate = 1e-3                                                                              
    drop = 0.3                                                                                        
    epochs_drop = 10.0                                                                                
    lrate = initial_lrate * math.pow(drop,((1+epoch)/epochs_drop))                                    
    return lrate    

