# coding=utf-8


import numpy as np
from numpy.random import RandomState
import os
from os import listdir
import nibabel as nib
import math
import csv
import random
import utils.brain_import_data as bid #da verificare
import pandas as pd
from sklearn import preprocessing
from sklearn.preprocessing import StandardScaler
import scipy.signal as si
from nilearn.plotting import plot_img
from nilearn.image import crop_img
from nilearn.image import resample_img

class DataLoader:
    """
    -load MRI data
    -separate data in train, validation and test set
    -compute tmap on train data
    -apply tmap to all MRI images
    """
    
    
    def __init__(self, target_shape, seed = None, kernel = None, aug = False):
        self.mri_control_datapath = 'master/control_data/'  # path to control_data images 
        self.mri_AD_datapath = 'master/AD_data/'            #path to AD_data images
        
        

        
        self.kernel = kernel
        self.target_shape = target_shape
        self.seed = seed
        self.train_dict = {} #used to compute mask.
        self.aug = aug

    def shuffle_dict_lists (self, dictionary): #modificata per aggiornamento V_1
        p = RandomState(self.seed).permutation(len(list(dictionary.values())[0]))  
        for key in dictionary.keys():
            dictionary[key] = [dictionary[key][i] for i in p]  

    
    def get_filenames (self):
        '''
        load filenames from a file .csv on csv_data directory
        '''
        control_list = listdir(self.mri_control_datapath)
        AD_list = listdir(self.mri_AD_datapath)
        
        control_list = list(filter (lambda x: 'cropped.nii.gz' in x, control_list)) #we used cropped images (eliminating slices of zeros) to reduce computational resources needed
        AD_list = list(filter (lambda x: 'cropped.nii.gz' in x, AD_list)) 

        #prendiamo gli AD superiori a 0.5
        AD_list_cdr = []
        df_AD = pd.read_csv('master/csv_data/AD_data.csv')
        for filename in AD_list:
            soggetto = filename[4:12]
            #soggetto = filename[10:18]

            if soggetto in list(df_AD[df_AD['cdr'] == 0.5]['soggetto']):#cdr==0.5 is very mild AD        
                AD_list_cdr.append(filename)       
        
        control_list_cdr = []
        df_control = pd.read_csv('master/csv_data/control_data.csv')
        for filename in control_list:
            soggetto = filename[4:12]
            #soggetto = filename[10:18]
            
            if soggetto in list(df_control[np.multiply(df_control['mmse']>28, df_control['cdr']< 0.5)]['soggetto']): 
        
                control_list_cdr.append(filename)
        
        
        control_dict = {'mri': control_list_cdr[:90]}          #change this two lines if you want increase dataset dimensions                                                                               
        AD_dict = {'mri': AD_list_cdr[:90]}
        
        
        
        print ('lunghezza control_dict', len(control_dict['mri']), 'lunghezza AD_dict', len(AD_dict['mri'])) 
        
            
        
        self.shuffle_dict_lists (control_dict) #fa lo shuffle dei due dizionari
        self.shuffle_dict_lists (AD_dict)
        
                    
        return control_dict, AD_dict
                
                    
    def split_filenames (self, control_dict, AD_dict, val_split=0.2):
        '''Split filename dictionaries in training/validation and test sets.
       
        
        '''
        #keys = ['JD', 'mri']
        keys = ['mri']
        train_dict, val_dict, test_dict = [{key: [] for key in keys } for _ in range(3)]#aggiunto

        

        num_test_samples =  int(math.ceil(val_split*(len(control_dict['mri']) + len(AD_dict['mri']))/2.0))

        num_val_samples =  int(val_split*(len(control_dict['mri']) + len(AD_dict['mri'])) - num_test_samples)
        print ('num_test samples', num_test_samples, 'num_val_samples',num_val_samples)#da cancellare
        
        for key in control_dict.keys():
            test_control = control_dict[key][:num_test_samples]
            test_AD = AD_dict[key][:num_test_samples]
            
            test_dict[key] = test_control + test_AD
            test_dict['health_state'] = np.concatenate( ( np.zeros( len(test_control) ),
                                             np.ones (   len(test_AD) ) ) )
            
            val_control = control_dict[key][num_test_samples : num_test_samples + num_val_samples]
            val_AD = AD_dict[key][num_test_samples : num_test_samples + num_val_samples]
            
            val_dict[key] = val_control + val_AD
            val_dict['health_state'] = np.concatenate( ( np.zeros( len(val_control) ),
                                             np.ones (   len(val_AD) ) ) )

            train_control_noise = []
            for filename in control_dict[key][num_test_samples + num_val_samples:]:
                train_control_noise.append(filename[:-7] + '_noise.nii.gz')                       
            train_control = control_dict[key][num_test_samples + num_val_samples:]
            
            train_AD_noise = []
            for filename in AD_dict[key][num_test_samples + num_val_samples:]:
                train_AD_noise.append(filename[:-7] + '_noise.nii.gz')                       
            train_AD = AD_dict[key][num_test_samples + num_val_samples:]
            
            if self.aug == True:
                train_dict[key] = train_control + train_control_noise + train_AD + train_AD_noise
                train_dict['health_state'] = np.concatenate( ( np.zeros( len(train_control) + len(train_control_noise) ),
                                             np.ones (   len(train_AD) + len(train_AD_noise) ) ) )
            
            elif self.aug == False:
                train_dict[key] = train_control + train_AD 
                train_dict['health_state'] = np.concatenate( ( np.zeros( len(train_control) ),
                                             np.ones (   len(train_AD) ) ) )
            
            self.train_dict[key] = train_control + train_AD
            self.train_dict['health_state'] = np.concatenate( ( np.zeros( len(train_control) ),
                                             np.ones (   len(train_AD) ) ) )
        
        


        return train_dict, val_dict, test_dict 
    
       
    
   
    
        
    def compute_maps(self, train_list, train_labels):
        '''
        it uses nistats to compute maps for randomized lists of train data. 
        return all maps
        '''
        from nistats.second_level_model import SecondLevelModel
        from nistats.thresholding import map_threshold
        n_subjects = int(len(train_list) / 2.0) 
        
        
        train_list2 =[]
        for i in train_list:
            if i in os.listdir(self.mri_control_datapath):
                train_list2.append(self.mri_control_datapath + i)
            elif i in os.listdir(self.mri_AD_datapath):
                train_list2.append(self.mri_AD_datapath + i)


        second_level_input = train_list2 
        condition_effect = -2 * np.array(train_labels, dtype =np.int) + 1  
        subject_effect = np.vstack((np.eye(n_subjects), np.eye(n_subjects)))
        subjects = ['S%02d' % i for i in range(1, n_subjects + 1)]
        design_matrix = pd.DataFrame(
            np.hstack((condition_effect[:, np.newaxis], subject_effect)),
            columns=['vertical vs horizontal'] + subjects)
        
        file_mask = os.listdir(self.mni_mask)
        
        second_level_model = SecondLevelModel(smoothing_fwhm = 6).fit(second_level_input, design_matrix = design_matrix)
        all_maps = second_level_model.compute_contrast('vertical vs horizontal',
                                            output_type='all')
        
        return all_maps
        

    def get_data_mri (self, filename_dict): 
        '''Loads subject volumes from filename dictionary
        Returns MRI volume and label     
        '''
        mris = np.zeros( (len(list(filename_dict.values())[0]),) +  self.target_shape)
        
        labels = filename_dict['health_state']
        print (self.seed)
        
        
       
        
        keys = ['mri']                                                               
        for key in keys:
            for j, filename in enumerate (filename_dict[key]):
                if key=='mri':
                    
                    if labels[j] == 0.:
                        proxy_image = nib.load(self.mri_control_datapath + '/' + filename)
                    elif labels[j] == 1.:
                        proxy_image = nib.load(self.mri_AD_datapath + '/' + filename)
                    
                    
                    
                    
                    image = proxy_image.dataobj
                    mris[j] = np.expand_dims(image, axis = -1)
                    

                    

                elif key=='JD':
                    
                    proxy_image = nib.load(self.jac_datapath + '/' + filename)
                    image = np.asarray(proxy_image.dataobj)
            
                    jacs[j] = np.asarray(np.expand_dims(image, axis = -1))
                    
                    
        return mris.astype('float32'), labels#, jacs.astype('float32'), labels

    def add_mask_to_mri (self, train_data, val_data, test_data, all_maps, threshold): 
        '''
        add mask in a separate proccess to mri in order to minimize waste of time in loading data for multiple evaluation of the same threshold 
        '''
                       
        test_mri, test_labels = test_data
        val_mri, val_labels = val_data
        train_mri, train_labels = train_data
        
        
        mask_plus = (all_maps['z_score'].dataobj > threshold) * 1.0 
        mask_minus = (all_maps['z_score'].dataobj < - threshold) * 1.0
        mask = (mask_plus + mask_minus) > 0.5 
        
        
        
        
        n_voxel = np.sum(mask)
        plot_img((nib.Nifti1Image(mask * 1.0, affine = np.eye(4))), title = 'plot ortoslice train mask NON CORRETTA CL, with t threshold = '+ str(threshold) + 'N voxel: ' + str(n_voxel), colorbar = True)
        nib.save(all_maps['stat'], os.path.join('master', 'last_tmap_'+ str(int(threshold * 100)) +'.nii.gz'))
        
        
        maschera = mask * 1.0 #V_1
        maschera_ampliata = si.correlate(maschera, self.kernel, mode = 'same') 
        maschera_ampliata = maschera_ampliata > 0.1
        n_voxel = np.sum(maschera_ampliata)
        plot_img((nib.Nifti1Image(maschera_ampliata * 1.0, affine = np.eye(4))), title = 'plot ortoslice train enlarged mask, with t threshold = '+ str(threshold)  + 'N voxel: ' + str(n_voxel), colorbar = True)
        maschera = np.asarray(np.expand_dims(maschera_ampliata * 1.0, axis = -1)) 

        for i in range(train_mri.shape[0]):
            train_mri[i] = np.multiply(train_mri[i], maschera)
        
        for i in range(val_mri.shape[0]):
            val_mri[i] = np.multiply(val_mri[i], maschera)

        for i in range(test_mri.shape[0]):
            test_mri[i] = np.multiply(test_mri[i], maschera)

        
        test_data = (test_mri, test_labels)
        val_data = (val_mri, val_labels)
        train_data = (train_mri, train_labels)
                  
                    
        return train_data, val_data, test_data, maschera

    
    

            
    def normalize_data (self, train_im, val_im, test_im, mode):
        '''
        We use different normalization procedures depending on data type
        '''
        if mode != 'mri' and mode != 'jac' and mode != 'xls':
            print ('Mode has to be mri, jac or xls depending on image data type')
        elif mode == 'mri':
            std = np.std(train_im, axis = 0) + 1.0 
            
            mean = np.mean (train_im, axis = 0)
            
            train_im = (train_im - mean)/(std + 1e-20)
            val_im = (val_im - mean)/(std + 1e-20)
            test_im = (test_im - mean)/(std + 1e-20)
        elif mode == 'jac':
            high = np.max(train_im)
            low = np.min(train_im)
            train_im = (train_im - low)/(high - low)
            val_im = (val_im - low)/(high - low)
            test_im = (test_im - low)/(high - low)
        else:
            high = np.max(train_im, axis = 0)
            low = np.min(train_im, axis = 0)
            train_im = (train_im - low)/(high - low)
            val_im = (val_im - low)/(high - low)
            test_im = (test_im - low)/(high - low)
        return train_im, val_im, test_im
            

    def get_train_val_test_dict (self, val_split = 0.2):
        control_dict, AD_dict = self.get_filenames()
        train_dict, val_dict, test_dict = self.split_filenames (control_dict = control_dict, AD_dict = AD_dict, val_split = 0.2)
        return train_dict, val_dict, test_dict
        
        
    def get_train_val_test (self, train_dict, val_dict, test_dict):
        train_mri, train_labels = self.get_data_mri(train_dict)
        val_mri, val_labels = self.get_data_mri(val_dict)
        test_mri, test_labels = self.get_data_mri(test_dict)


        train_mri, val_mri, test_mri = self.normalize_data (train_mri, val_mri, test_mri, mode = 'mri')
        
        
        test_data = (test_mri, test_labels)
        val_data = (val_mri, val_labels)
        train_data = (train_mri, train_labels)
    
        
        return train_data, val_data, test_data

   
