This repository contains the code developed for the article:

"Voxel-wise feature selection method for CNN binary classification of neuroimaging data"
Domenico Messina, Pasquale Borrelli, Paolo Russo, Marco Salvatore, Marco Aiello

We provide our experimentation code for control (NC) vs very-mild Alzheimer's disease (AD) subjects classification task .

utils contain:
    - Out data loading and preprocessing procedures for the structural MRI and t-masking procedures(preprocess.py)
    - The data iterator (augmentation.py)
    - A Spasov et.al (2019) implementation of 3D separable convolutions, updated for python 3 and tensorflow 2.x (sepconv3D.py)
    - The network model architecture (models.py)

main_run.py runs our evaluation experiments

To run the code in cloud, you can follow these steps:
    - download the repository on your google drive account
    - add AD MRI images in AD_data
    - add NC MRI images in control_data
    - add two csv file (one for AD and one for NC) which contain the following clinical data for each subject: MMSE, CDR
    - open the colab notebook main_run.ipynb and run all cells (using the run time with gpu enabled).


