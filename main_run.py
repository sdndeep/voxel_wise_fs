# -*- coding: utf-8 -*-




import os
import numpy as np
from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score
from sklearn.metrics import confusion_matrix     
from keras import regularizers 
from utils.preprocess import DataLoader
from utils.models import Parameters, Net
from matplotlib import pyplot as plt
from matplotlib.pyplot import matshow
from nilearn.plotting import plot_img
import nibabel as nib
import pandas as pd
import keras.backend as K


target_rows = 148
target_cols = 192
depth = 156
axis = 1
num_clinical = 3
drop_rate = 0.1
w_regularizer = regularizers.l2(5e-5)
batch_size = 6
EPOCHS=150


model_filepath = '../path'


params_dict = { 'w_regularizer': w_regularizer, 'batch_size': batch_size,
               'drop_rate': drop_rate, 'epochs': EPOCHS,
          'gpu': "/gpu:0", 'model_filepath': model_filepath, 
          'image_shape': (target_rows, target_cols, depth, axis),
          'num_clinical':num_clinical
          }
paramss = []
 
for i in range(1):#questo ciclo serve per allenare modelli con un numero di epoche EPOCHS - i
  params_dict['epochs'] = EPOCHS - i 
  params = Parameters(params_dict)
  paramss.append(params)

numero_di_ripetizioni_casuali=5
seeds = [np.random.randint(1, 5000) for _ in range(numero_di_ripetizioni_casuali)]
print (seeds)

def training(train_data, val_data, test_data):
    histories = []
    models = {}
    i = 0
    for param in paramss:  
      
      net = Net(param)
      models[i] = net
      i+=1
      history = net.train((train_data, val_data))
      histories.append(history)
    
        #need to nnormalize and arrange my test data (or train data as when training)
      test_loss, test_acc = net.evaluate (test_data)# la variabile _ rappresenta il learning rate, poichè è stato inserito come metrica
      print  ('test_acc', test_acc)
      print (history.keys())
      #plt.plot(history['lr'])
      #plt.title('learning rate')
      #plt.xlabel('epochs')
      #plt.show()
      plt.plot(history['loss'])
      plt.plot(history['val_loss'])
      plt.xlabel('epochs')
      plt.ylabel('loss')
      plt.title('train-val loss')
      plt.legend(['train loss', 'val loss'], loc='upper left')
      plt.show()
      plt.plot(history['acc'])
      plt.plot(history['val_acc'])
      #plt.plot(test_acc)
      plt.xlabel('epochs')
      plt.ylabel('accuracy')
      plt.title('train-val accuracy')
      plt.legend(['train acc', 'val acc'], loc='upper left')
      
      plt.show()
      test_preds = net.predict(test_data)
      print (test_preds)  
      fpr_test, tpr_test, thresholds_test = roc_curve(test_data[-1], test_preds)
      
      auc_test = roc_auc_score(test_data[-1], test_preds) 
      print (' auc test:',  auc_test)
      
      plt.plot(fpr_test, tpr_test)
      plt.xlabel('falsi positivi test')
      plt.ylabel('veri positivi test')
      #plt.show()
      
      #tn, fp, fn, tp = confusion_matrix(y_true = test_data[-1], y_pred = np.round(test_preds))/(float(len(test_data[-1]))/2) 
      print (confusion_matrix(y_true = test_data[-1], y_pred = np.round(test_preds))/(float(len(test_data[-1]))/2)) 
      

      #print 'test confusion: (tn, fp, fn, tp) = ', tn, fp, fn, tp 
      
      val_loss, val_acc = net.evaluate (val_data)
      
      val_preds = net.predict(val_data) 
      
      fpr_val, tpr_val, thresholds_val = roc_curve(val_data[-1], val_preds)
      auc_val= roc_auc_score(val_data[-1], val_preds) 
      print (' auc val:',  auc_val)
      
      plt.plot(fpr_val, tpr_val)
      plt.xlabel('falsi positivi val')
      plt.ylabel('veri positivi val')
      plt.title('test-val roc')
      plt.legend(['test roc', 'val roc'], loc='upper left')
    
      plt.show()

    

      print (confusion_matrix(y_true = val_data[-1], y_pred = np.round(val_preds))/(float(len(val_data[-1]))/2)) 
      
      
     
    
    K.clear_session()
    return histories, models, auc_val, auc_test, test_acc


def evaluate_norm_xls_net (seed, espandi_maschera = 1, aug = False):
    #threshold = soglia superiore del pvalue
    risultati = {}
    histories = []
    esp = espandi_maschera
    data_loader = DataLoader((target_rows, target_cols, depth, axis), seed = seed, kernel = np.ones([esp, esp, esp]), aug = aug)
  
    train_dict, val_dict, test_dict = data_loader.get_train_val_test_dict(val_split=0.2)
    
    
    f = open('master/dic_soggetti_label/train_dict_' + str(seed), 'w')
    f.write(str(train_dict))
    f.close()

    f = open('master/dic_soggetti_label/val_dict_' + str(seed), 'w')
    f.write(str(val_dict))
    f.close()

    f = open('master/dic_soggetti_label/test_dict_' + str(seed), 'w')
    f.write(str(test_dict))
    f.close()


    all_maps = data_loader.compute_maps(data_loader.train_dict['mri'], train_labels = data_loader.train_dict['health_state'])
    plot_img(all_maps['stat'], title = 'plot ortoslice train tmap', colorbar = True)
    
    train_data, val_data, test_data = data_loader.get_train_val_test(train_dict, val_dict, test_dict)
    
    for threshold in list(np.linspace(0,8, num = 21)):
      
      train_data, val_data, test_data, maschera = data_loader.add_mask_to_mri(train_data, val_data, test_data, all_maps, threshold)
      
      n_voxel = np.sum(maschera)
      plot_img((nib.Nifti1Image(maschera, affine = np.eye(4))), title = 'plot ortoslice train enlarged mask, with t threshold = '+ str(threshold)  + 'N voxel: ' + str(n_voxel), colorbar = True)
    
      data = train_data, val_data, test_data
      history, models, auc_val, auc_test, test_acc = training (train_data, val_data, test_data)
      i = np.where(history[0]['val_loss'] == np.min(history[0]['val_loss']))
      risultati[(str(round(threshold, 2)), seed)] = {'train_acc' : history[0]['acc'][i[0][0]], 'val_acc': history[0]['val_acc'][i[0][0]], 'test_acc': test_acc, 'auc_val':auc_val,'auc_test':auc_test}
      histories.append(history)  
      
    
    
    #nib.save(tmap, os.path.join('master', 'tmap1607_' + str(seed) +'.nii.gz'))
    
    return risultati, histories#, models, data

def shape_for_cropping():
    from nilearn.image import crop_img
    data_loader = DataLoader((target_rows, target_cols, depth, axis), seed = 1, kernel = np.ones([1, 1, 1]), aug = False)
    control_dict, AD_dict = data_loader.get_filenames()
    shapes = []
    for filename in control_dict['mri']:
      proxy_image = crop_img(nib.load(data_loader.mri_control_datapath + '/' + filename ))
      shapes.append(proxy_image.shape)

    for filename in AD_dict['mri']:
      proxy_image = crop_img(nib.load(data_loader.mri_AD_datapath + '/' + filename ))
      shapes.append(proxy_image.shape)
    return shapes


def evaluate_norm_plus_gaussian_noise_net (seed, espandi_maschera = 1, aug = False):
    
    risultati = {}
    histories = []
    esp = espandi_maschera
    data_loader = DataLoader((target_rows, target_cols, depth, axis), seed = seed, kernel = np.ones([esp, esp, esp]), aug = aug)
  
    train_dict, val_dict, test_dict = data_loader.get_train_val_test_dict(val_split=0.2)
    
    
    f = open('master/dic_soggetti_label/train_dict_' + str(seed), 'w')
    f.write(str(train_dict))
    f.close()

    f = open('master/dic_soggetti_label/val_dict_' + str(seed), 'w')
    f.write(str(val_dict))
    f.close()

    f = open('master/dic_soggetti_label/test_dict_' + str(seed), 'w')
    f.write(str(test_dict))
    f.close()


    all_maps = data_loader.compute_maps(data_loader.train_dict['mri'], train_labels = data_loader.train_dict['health_state'])
    
    np.random.RandomState(seed = seed)
    noise = np.random.randn(target_rows, target_cols, depth)                        
    affine = all_maps['z_score'].affine
    new_tmap = all_maps['z_score'].dataobj + 0.2 * noise
    all_maps['z_score'] = nib.Nifti1Image(new_tmap, affine= affine)
        
    
    plot_img(all_maps['z_score'], title = 'plot ortoslice train z_score', colorbar = True)
    
    train_data, val_data, test_data = data_loader.get_train_val_test(train_dict, val_dict, test_dict)
    
    for threshold in list(np.linspace(0,8, num = 21)):
      
      train_data, val_data, test_data, maschera = data_loader.add_mask_to_mri(train_data, val_data, test_data, all_maps, threshold)
      
      n_voxel = np.sum(maschera)
      plot_img((nib.Nifti1Image(maschera, affine = np.eye(4))), title = 'plot ortoslice train enlarged mask, with t threshold = '+ str(threshold)  + 'N voxel: ' + str(n_voxel), colorbar = True)
    
      data = train_data, val_data, test_data
      history, models, auc_val, auc_test, test_acc = training (train_data, val_data, test_data)
      i = np.where(history[0]['val_loss'] == np.min(history[0]['val_loss']))
      risultati[(str(round(threshold, 2)), seed)] = {'train_acc' : history[0]['acc'][i[0][0]], 'val_acc': history[0]['val_acc'][i[0][0]], 'test_acc': test_acc, 'auc_val':auc_val,'auc_test':auc_test}
      histories.append(history)  
      
    
    
      #nib.save(tmap, os.path.join('master', 'tmap1607_' + str(seed) +'.nii.gz'))
    
    return risultati, histories#, models, data

def evaluate_norm_minus_total_random_net (seed, espandi_maschera = 1, aug = False):
    
    risultati = {}
    histories = []
    esp = espandi_maschera
    data_loader = DataLoader((target_rows, target_cols, depth, axis), seed = seed, kernel = np.ones([esp, esp, esp]), aug = aug)
  
    train_dict, val_dict, test_dict = data_loader.get_train_val_test_dict(val_split=0.2)
    
    
    f = open('master/dic_soggetti_label/train_dict_' + str(seed), 'w')
    f.write(str(train_dict))
    f.close()

    f = open('master/dic_soggetti_label/val_dict_' + str(seed), 'w')
    f.write(str(val_dict))
    f.close()

    f = open('master/dic_soggetti_label/test_dict_' + str(seed), 'w')
    f.write(str(test_dict))
    f.close()


    all_maps = data_loader.compute_maps(data_loader.train_dict['mri'], train_labels = data_loader.train_dict['health_state'])
    
    np.random.RandomState(seed = seed)
    noise = np.random.rand(target_rows, target_cols, depth) > 0.05                       
    affine = all_maps['z_score'].affine
    new_tmap = np. multiply(all_maps['z_score'].dataobj, noise * 1.0)
    all_maps['z_score'] = nib.Nifti1Image(new_tmap, affine= affine)
        
    
    plot_img(all_maps['z_score'], title = 'plot ortoslice train z_score', colorbar = True)
    
    train_data, val_data, test_data = data_loader.get_train_val_test(train_dict, val_dict, test_dict)
    
    
    for threshold in list(np.linspace(0,8, num = 21)):
      
      train_data, val_data, test_data, maschera = data_loader.add_mask_to_mri(train_data, val_data, test_data, all_maps, threshold)
      
      n_voxel = np.sum(maschera)
      plot_img((nib.Nifti1Image(maschera, affine = np.eye(4))), title = 'plot ortoslice train enlarged mask, with t threshold = '+ str(threshold)  + 'N voxel: ' + str(n_voxel), colorbar = True)
    
      data = train_data, val_data, test_data
      history, models, auc_val, auc_test, test_acc = training (train_data, val_data, test_data)
      i = np.where(history[0]['val_loss'] == np.min(history[0]['val_loss']))#trova l'epoca con la prestazione migliore in base alle caratteristiche del early stopping con restore_weights = True
      risultati[(str(round(threshold, 2)), seed)] = {'train_acc' : history[0]['acc'][i[0][0]], 'val_acc': history[0]['val_acc'][i[0][0]], 'test_acc': test_acc, 'auc_val':auc_val,'auc_test':auc_test}
      histories.append(history)  
      
    
    
      #nib.save(tmap, os.path.join('master', 'tmap1607_' + str(seed) +'.nii.gz'))
    
    return risultati, histories#, models, data



def evaluate_norm_plus_valdata_net (seed, espandi_maschera = 1, aug = False):
    
    risultati = {}
    histories = []
    esp = espandi_maschera
    data_loader = DataLoader((target_rows, target_cols, depth, axis), seed = seed, kernel = np.ones([esp, esp, esp]), aug = aug)
  
    train_dict, val_dict, test_dict = data_loader.get_train_val_test_dict(val_split=0.2)
    
    
    f = open('master/dic_soggetti_label/train_dict_' + str(seed), 'w')
    f.write(str(train_dict))
    f.close()

    f = open('master/dic_soggetti_label/val_dict_' + str(seed), 'w')
    f.write(str(val_dict))
    f.close()

    f = open('master/dic_soggetti_label/test_dict_' + str(seed), 'w')
    f.write(str(test_dict))
    f.close()


    all_maps = data_loader.compute_maps(data_loader.train_dict['mri']+ val_dict['mri'], train_labels = np.concatenate((data_loader.train_dict['health_state'], val_dict['health_state'])))# separo il processo di aggiunta maschera per velocizzare i cicli su più immagini
    plot_img(all_maps['stat'], title = 'plot ortoslice train tmap', colorbar = True)
    
    train_data, val_data, test_data = data_loader.get_train_val_test(train_dict, val_dict, test_dict)
    
    
    
    for threshold in list(np.linspace(0,8, num = 21)):
      
      train_data, val_data, test_data, maschera = data_loader.add_mask_to_mri(train_data, val_data, test_data, all_maps, threshold)
      
      n_voxel = np.sum(maschera)
      plot_img((nib.Nifti1Image(maschera, affine = np.eye(4))), title = 'plot ortoslice train enlarged mask, with t threshold = '+ str(threshold)  + 'N voxel: ' + str(n_voxel), colorbar = True)
    
      data = train_data, val_data, test_data
      history, models, auc_val, auc_test, test_acc = training (train_data, val_data, test_data)
      i = np.where(history[0]['val_loss'] == np.min(history[0]['val_loss']))
      risultati[(str(round(threshold, 2)), seed)] = {'train_acc' : history[0]['acc'][i[0][0]], 'val_acc': history[0]['val_acc'][i[0][0]], 'test_acc': test_acc, 'auc_val':auc_val,'auc_test':auc_test}
      histories.append(history)  
      
    
    
    #nib.save(tmap, os.path.join('master', 'tmap1607_' + str(seed) +'.nii.gz'))
    
    return risultati, histories#, models, data


def evaluate_mask_all_data_net (seed, espandi_maschera = 1, aug = False):
    
    risultati = {}
    histories = []
    esp = espandi_maschera
    data_loader = DataLoader((target_rows, target_cols, depth, axis), seed = seed, kernel = np.ones([esp, esp, esp]), aug = aug)
  
    train_dict, val_dict, test_dict = data_loader.get_train_val_test_dict(val_split=0.2)
    
    
    f = open('master/dic_soggetti_label/train_dict_' + str(seed), 'w')
    f.write(str(train_dict))
    f.close()

    f = open('master/dic_soggetti_label/val_dict_' + str(seed), 'w')
    f.write(str(val_dict))
    f.close()

    f = open('master/dic_soggetti_label/test_dict_' + str(seed), 'w')
    f.write(str(test_dict))
    f.close()

    new_labels = np.concatenate((data_loader.train_dict['health_state'], np.concatenate((val_dict['health_state'], test_dict['health_state']))))
    new_mri_data = np.concatenate((data_loader.train_dict['mri'], np.concatenate((val_dict['mri'], test_dict['mri']))))

    all_maps = data_loader.compute_maps(new_mri_data, train_labels = new_labels)# separo il processo di aggiunta maschera per velocizzare i cicli su più immagini
    plot_img(all_maps['stat'], title = 'plot ortoslice train tmap', colorbar = True)
    
    train_data, val_data, test_data = data_loader.get_train_val_test(train_dict, val_dict, test_dict)
    
    for threshold in list(np.linspace(0,8, num = 21)):
      
      train_data, val_data, test_data, maschera = data_loader.add_mask_to_mri(train_data, val_data, test_data, all_maps, threshold)
      
      n_voxel = np.sum(maschera)
      plot_img((nib.Nifti1Image(maschera, affine = np.eye(4))), title = 'plot ortoslice train enlarged mask, with t threshold = '+ str(threshold)  + 'N voxel: ' + str(n_voxel), colorbar = True)
    
      data = train_data, val_data, test_data
      history, models, auc_val, auc_test, test_acc = training (train_data, val_data, test_data)
      i = np.where(history[0]['val_loss'] == np.min(history[0]['val_loss']))
      risultati[(str(round(threshold, 2)), seed)] = {'train_acc' : history[0]['acc'][i[0][0]], 'val_acc': history[0]['val_acc'][i[0][0]], 'test_acc': test_acc, 'auc_val':auc_val,'auc_test':auc_test}
      histories.append(history)  
      
    
    
    #nib.save(tmap, os.path.join('master', 'tmap1607_' + str(seed) +'.nii.gz'))
    
    return risultati, histories#, models, data


def evaluate_net (seed, espandi_maschera = 1):
    #threshold = soglia superiore del pvalue
    risultati = {}
    histories = []
    esp = espandi_maschera
    data_loader = DataLoader((target_rows, target_cols, depth, axis), seed = seed, kernel = np.ones([esp, esp, esp]))
  
    train_dict, val_dict, test_dict = data_loader.get_train_val_test_dict(val_split=0.2)
    train_data, val_data, test_data = data_loader.get_train_val_test(train_dict, val_dict, test_dict)
      
    data = train_data, val_data, test_data
    history, models, auc_val, auc_test, test_acc = training (train_data, val_data, test_data)
    i = np.where(history[0]['val_loss'] == np.min(history[0]['val_loss']))
    risultati[('NO threshold', seed)] = {'train_acc' : history[0]['acc'][i[0][0]], 'val_acc': history[0]['val_acc'][i[0][0]], 'test_acc': test_acc, 'auc_val':auc_val,'auc_test':auc_test}#(str(round(threshold, 2))
    histories.append(history)
      #nib.save(tmap, os.path.join('master', 'tmap1607_' + str(seed) +'.nii.gz'))

     
    return risultati, histories#, models, data

results = {}
dic_histories = {}
for seed in seeds:
    
  result, histories = evaluate_norm_xls_net(seed,espandi_maschera=1)#You must change this function if you want to select another experiment type
  dic_histories[seed] = histories
  for key in result.keys():
    results[key] = result[key]
  df = pd.DataFrame(results) 
  df.to_csv(os.path.join('master//risultati','OASIS_MNI_masked_experimental_data.csv'))

  df_histories = pd.DataFrame(dic_histories)
  df.to_csv(os.path.join('master//risultati','OASIS_MNI_masked_experimental_histories.csv'))
